package com.example.moneymanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class StanjeFragment extends Fragment {
    private Context con;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stanje, container, false);

        getActivity().setTitle("Money Manager");

        con = getContext();
        DatabaseHelper databaseHelper = new DatabaseHelper(con);
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        TextView got = view.findViewById(R.id.stanjeGotovina);
        TextView kar = view.findViewById(R.id.stanjeKartice);
        String upit = "SELECT _id, stanje FROM RACUNI";
        Cursor c = db.rawQuery(upit, null);
        if ((c != null) && (c.getCount() > 0)) {
            c.moveToFirst();
            got.setText(Float.toString(c.getFloat(c.getColumnIndex("stanje"))) + " kn");
            c.moveToNext();
            kar.setText(Float.toString(c.getFloat(c.getColumnIndex("stanje"))) + " kn");
        }
        else {
            ContentValues pocValues = new ContentValues();
            pocValues.put("stanje", 0);
            db.insert("RACUNI", null, pocValues);
            db.insert("RACUNI", null, pocValues);
            got.setText("0 kn");
            kar.setText("0 kn");
        }
        c.close();
        db.close();

        return view;
    }
}
