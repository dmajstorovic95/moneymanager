package com.example.moneymanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new StanjeFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_stanje);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.nav_stanje:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new StanjeFragment()).commit();
                break;
            case R.id.nav_trans:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new TransFragment()).commit();
                break;
            case R.id.nav_povijest:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new PovijestFragment()).commit();
                break;
            case R.id.nav_reset:
                reset();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void reset(){
        this.deleteDatabase("money.db");
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new StanjeFragment()).commit();
        Toast.makeText(this, "Resetirano", Toast.LENGTH_SHORT).show();
    }
}
