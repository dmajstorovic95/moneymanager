package com.example.moneymanager;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.moneymanager.pojo.Transakcija;


public class PovijestAdapter extends CursorAdapter {
    private Cursor cur;
    public PovijestAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
        cur = cursor;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.fragment_povijest, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView iznos = view.findViewById(R.id.iznosPov);
        TextView vrsta = view.findViewById(R.id.vrstaPov);
        TextView opis = view.findViewById(R.id.opisPov);
        TextView datum = view.findViewById(R.id.datumPov);

        float vrijIznos = cursor.getFloat(cursor.getColumnIndex("iznos"));
        int vrijVrsta = cursor.getInt(cursor.getColumnIndex("idRacun"));
        String vrijOpis = cursor.getString(cursor.getColumnIndex("opis"));
        String vrijDatum = cursor.getString(cursor.getColumnIndex("datum"));

        iznos.setText(String.valueOf(vrijIznos));
        if (vrijVrsta == 1)
            vrsta.setText("Gotovina");
        else vrsta.setText("Kartice");
        opis.setText(vrijOpis);
        datum.setText(vrijDatum);
    }

}