package com.example.moneymanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "money.db";
    private static int DATABASE_VERSION = 1;
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String CREATE_TABLE_RACUNI = "create table racuni ("
                +"_id" + " integer primary key autoincrement, "
                +"stanje" + " real)";
        sqLiteDatabase.execSQL(CREATE_TABLE_RACUNI);

        String CREATE_TABLE_TRANS = "create table trans ("
                +"_id" + " integer primary key autoincrement, "
                +"idRacun" + " integer not null, "
                +"datum" + " text not null, "
                +"iznos" + " real not null, "
                +"opis" + " text)";
        sqLiteDatabase.execSQL(CREATE_TABLE_TRANS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table KORISNIK");
        sqLiteDatabase.execSQL("drop table RACUNI");
        sqLiteDatabase.execSQL("drop table TRANS");
        Log.i("izmjena verzije", "dropa tablice");
        onCreate(sqLiteDatabase);
    }
}
