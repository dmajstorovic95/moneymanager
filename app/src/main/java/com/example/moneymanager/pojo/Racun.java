package com.example.moneymanager.pojo;

public class Racun {
    private int id;
    private String tip;
    private float iznos;

    public Racun(int id, String tip, float iznos) {
        this.id = id;
        this.tip = tip;
        this.iznos = iznos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public float getIznos() {
        return iznos;
    }

    public void setIznos(float iznos) {
        this.iznos = iznos;
    }
}
