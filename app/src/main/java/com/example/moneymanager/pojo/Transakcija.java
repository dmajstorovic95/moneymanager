package com.example.moneymanager.pojo;

public class Transakcija {
    private int id;
    private int idRacun;
    private String datum;
    private float iznos;
    private String opis;

    public Transakcija(){}

    public Transakcija(int id, int idRacun, String datum, float iznos, String opis) {
        this.id = id;
        this.idRacun = idRacun;
        this.datum = datum;
        this.iznos = iznos;
        this.opis = opis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdRacun() {
        return idRacun;
    }

    public void setIdRacun(int idRacun) {
        this.idRacun = idRacun;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public float getIznos() {
        return iznos;
    }

    public void setIznos(float iznos) {
        this.iznos = iznos;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
}
