package com.example.moneymanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;


public class TransFragment extends Fragment {
    private Context con;
    private int tip;
    private EditText iznos, datum, opis;
    private RadioGroup radioGroup;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trans, container, false);

        getActivity().setTitle("Nova transakcija");

        con = getContext();
        iznos = view.findViewById(R.id.iznosText);
        opis = view.findViewById(R.id.opisText);
        datum = view.findViewById(R.id.datumText);
        Button spremi = view.findViewById(R.id.spremi);
        radioGroup = view.findViewById(R.id.tipNovca);

        datum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                birajDatum(view);
            }
        });
        spremi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unos(view);
            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.cash)
                    tip = 1;
                else
                    tip = 2;
            }
        });
        return view;
    }

    public void birajDatum (View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public void unos (View view) {
        if (iznos.getText().toString().length() < 1 ||
                opis.getText().toString().length() < 1 ||
                datum.getText().toString().length() < 1 ||
                tip > 2 || tip < 1){
                    Toast.makeText(con,"Morate sve unijeti", Toast.LENGTH_SHORT).show();
                    return;
        }
        DatabaseHelper databaseHelper = new DatabaseHelper(con);
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues transValues = new ContentValues();
        transValues.put("idRacun", tip);
        transValues.put("datum", datum.getText().toString());
        transValues.put("iznos", iznos.getText().toString());
        transValues.put("opis", opis.getText().toString());
        long rowid = db.insert("TRANS", null, transValues);
        if (rowid <= 0){
            Toast.makeText(con,"Nije uspjelo!",Toast.LENGTH_SHORT).show();
            db.close();
            return;
        }
        db.execSQL("UPDATE RACUNI SET stanje = stanje + " + Float.valueOf(iznos.getText().toString()) +" WHERE _id = " + tip);
        Toast.makeText(con,"Unos uspješan! "+rowid,Toast.LENGTH_SHORT).show();
        db.close();
    }
}
